# Docker image file forci pipeline

This is a project for building docker image, using in Bitbucket pipeline.

## Tools

The main tools:

* based image  maven:3.6.0-jdk-8
* OpenJDK **1.8.0_181**
* Maven **3.6.0** 
* Ruby **2.3.3p222** 
* Ruby Sass **3.7.3**

## How to build

To build :

    docker build .
    